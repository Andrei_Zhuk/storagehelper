storageHelper - JavaScript-библиотека, которая позволит без траты лишнего времени манипулировать с данными локального хранилища браузера.
Для работы использует localStorage и не требует подключения jQuery.
Browser support: http://caniuse.com/#search=localStorage

storageHelper.set - Записывает ключ:значение в локальное хранилище.
    Example: storageHelper.set('YOUR_KEY', 'YOUR_VALUE');

storageHelper.setArray - Записывает набор данных, состоящих из ключ:значение
    Example: storageHelper.setArray({'YOUR_KEY' : 'YOUR_VALUE', 'YOUR_KEY' : 'YOUR_VALUE', 'YOUR_KEY' : 'YOUR_VALUE'});

storageHelper.get - Получает значение по ключу, либо же выводит массив всех ключ:значение (Эквивалентно .getAll()).
        Example: storageHelper.get('YOUR_KEY');

storageHelper.getAll - Выводит массив всех ключ:значение.
        Example: storageHelper.getAll();

storageHelper.is - Проверяет, находится ли ключ со значением в локальном хранилище. Возвращает значение или false в соответствующих случаях, а так же обладает callback-ом, как в отрицательном, так и в положительном случае.
        Example:
            storageHelper.is('YOUR_KEY');
        Example with callbacks:
            storageHelper.is('YOUR_KEY', function(){ YOUR_CODE }, function(){ YOUR_CODE } );

storageHelper.del - Удаляет значение из хранилища
        Example: storageHelper.del('YOUR_KEY');



===/ Real example:
    // Инициализируем библиотеку для дальнейшей работы.
    var storageHelper = new storageHelper();

    // Проверяем, если не существует ключа 'seotoaster', создаем его и выводим сообщение.
    storageHelper.is('seotoaster', function(){}, function(){
        storageHelper.set('seotoaster', 'good');
        console.log('Hav`ent. Created');
    });

    // Выводим в консоль значения только что созданного ключа.
    console.log(storageHelper.get('seotoaster'));

    // Проверяем, если существует ключ, то удаляем его и выводим сообщение.
    storageHelper.is('seotoaster', function(){
        storageHelper.del('seotoaster');
        console.log('Have. Removed');
    });