/**
 * Created by Andrei Zhuk on 20.04.2015.
 */

!function (name, definition) {
	if (typeof module != 'undefined') module.exports = definition()
	else if (typeof define == 'function' && typeof define.amd == 'object') define(definition)
	else this[name] = definition()
}('storageHelper', function () {

	var storageHelper = function () {
		this._ = {};
	};

	storageHelper.prototype = {
		// base methods
		set: function (key, value) {
			if (!value) value = 0;
			localStorage.setItem(key, value);
			return this;
		},
		setArray: function (data) {
			for (var key in data) {
				if (!data.hasOwnProperty(key)) {
					continue;
				}
				localStorage.setItem(key, data[key]);
			}
			return this;
		},
		get: function (key) {
			if (key && key != '') {
				if (localStorage.getItem(key)) {
					return localStorage.getItem(key);
				} else {
					return this;
				}
			} else {
				return this.getAll();
			}
		},
		getAll: function () {
			var archive = {}, keys = Object.keys(localStorage);
			for (var i = 0; key = keys[i]; i++) {
				archive[key] = localStorage.getItem(key);
			}
			return archive;
		},
		del: function (key) {
			if (key !== '') {
				if (localStorage.getItem(key)) {
					localStorage.removeItem(key);
					return this;
				} else {
					return this;
				}
			}
		},
		is: function (key, callbackSuccess, callbackError) {
			if (key !== '') {
				if (localStorage.getItem(key) != null) {
					if (callbackSuccess) callbackSuccess();
					return localStorage.getItem(key);
				} else {
					if (callbackError) callbackError();
					return false;
				}
			}
		}
	};
	return storageHelper;
});